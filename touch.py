######## Touch
## Python implementation of unix touch command
##
## Supports -r and -v options for recursive touch
## and verbose output respectively
##
## Author: Justin Brooks
#########

import sys
import os

def touch(fname, times=None):
    if os.path.exists(fname):
        os.utime(fname, times)
    else:
        # create file
        f = open(fname, 'a')
        f.close()

def dir_contents(path):
    files, folders = [],[]

    folders += [path]
    
    for dirpath, dirnames, filenames in os.walk(path):
        for name in filenames:
            files += [os.path.join(dirpath,name)]
        for name in dirnames:
            folders += [os.path.join(dirpath,name)]
    
    
    return files, folders

def main():

    options = []
    recursive = False
    verbose = False
    gotPath = False
    path = ""
    touchmsg = "\tTouching: "
    
    for string in sys.argv[1:]:
        ## collect options
        if "\\" in string or "-" in string:
            options += [string]
        else:
            if not gotPath:
                path = string
                gotPath = True

    ## Check for recursive option
    for option in options:
        if 'r' in option.lower():
            recursive = True
        if 'v' in option.lower():
            verbose = True
            
            #print blank line to make output readable
            print() 
            
    ## Only continue if we got a filepath
    if gotPath:
        if recursive:
            files, folders = dir_contents(path)
            for file in files:
                if verbose:
                    print(touchmsg, file)
                touch(file)
            for folder in folders:
                if verbose:
                    print(touchmsg, folder)
                touch(folder)

        else:
            if verbose:
                print(touchmsg, path)
            touch(path)

main()
