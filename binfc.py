##############################################################################
##
## Binary File Checking Script
## Justin Brooks
## Sept 24, 2014
##
## usage:
##      binfc.py file1 file2
##
##      binfc.py -hashalg file1 file2
##              where hashalg is a string representing
##              a hashing algorithm i.e. sha1, md5
##
## * character indicates both hashes have the same character at that position
##
###############################################################################

import hashlib
import sys

def DisplayDifferences(FilesDiffer, hash1, hash2, hash_option):

    if FilesDiffer:
        hashdif1 = ""
        hashdif2 = ""
        print()
        print("The files differ.")
        print()
        for i in range(len(hash1)):
            if (hash1[i] != hash2[i]):
                hashdif1 += hash1[i]
                hashdif2 += hash2[i]
            else:
                hashdif1 += '*'
                hashdif2 += '*'
        print("File 1 " + hash_option + " hash: \n\t\t" + hashdif1)
        print("File 2 " + hash_option + " hash: \n\t\t" + hashdif2)
        
    else:
        print()
        print("The files are identical.")
        print()
        print("File 1 " + hash_option + " hash: \n\t\t" + hash1)
        print("File 2 " + hash_option + " hash: \n\t\t" + hash2)

def Hash(hash_option, f1, f2):

    bAreDifferent = False
    hash = hashlib.new(hash_option)

    ## File 1
    with open(f1, "rb+") as f:
        byte = f.read(1)
        while byte:
            hash.update(byte)
            byte = f.read(1)
            
    file1_hash = hash.hexdigest()

    hash = hashlib.new(hash_option)

    ## File 2
    with open(f2, "rb+") as f:
        byte = f.read(1)
        while byte:
            hash.update(byte)
            byte = f.read(1)

    file2_hash = hash.hexdigest()
    
    if (file1_hash != file2_hash):
        bAreDifferent = True

    return bAreDifferent, file1_hash, file2_hash

    
def main():

    strUsage = "\n## Usage:  \n##\n## binfc.py file1 file2\n##\n## binfc.py -hashalg file1 file2\n## \
--where hashalg is a string representing\n## --a hashing algorithm\n## --sha1, md5, sha224, sha256, sha384, or sha512"

    strHashAlg = "sha1"
    file1 = None
    file2 = None

    file1_hash = None
    file2_hash = None

    if (len(sys.argv) < 3):
        print(strUsage)
        exit()
        
    #check number of command line args
    #if 2 args default to sha1
    if (len(sys.argv) == 4):
        if (sys.argv[1].startswith('-') or sys.argv[1].startswith('\\')):
            strHashAlg = sys.argv[1][1:]
            file1 = sys.argv[2]
            file2 = sys.argv[3]
        else:
            print("Error: please use '\\' or '-' prefix on hash option\n")
            exit()
    #if the hash option isn't specified
    elif (len(sys.argv) == 3 and ("\\" not in sys.argv[1]) \
          and ("-" not in sys.argv[1])):
        file1 = sys.argv[1]
        file2 = sys.argv[2]
    else:
        print(strUsage)
        exit()

    hashinfo = Hash(strHashAlg, file1, file2)

    FilesDiffer = hashinfo[0]
    File1Hash   = hashinfo[1]
    File2Hash   = hashinfo[2]
    
    DisplayDifferences(FilesDiffer, File1Hash, File2Hash, strHashAlg)

main()
