import sys

def main():
    count = 0
    for line in sys.stdin:
        count += 1

    print("Results: " + str(count))
    
main()
